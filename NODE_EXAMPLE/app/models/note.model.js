const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    Name : String,
    Position : String,
    Office : String,
    Age : Number,
    StartDate : Date,
    Salary : Number
}, {
    versionKey: false
});

module.exports = mongoose.model('Note', NoteSchema);